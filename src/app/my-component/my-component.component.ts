import { Component, OnInit } from '@angular/core';
import { MyServService } from '../my-serv.service';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-my-component',
  templateUrl: './my-component.component.html',
  styleUrls: ['./my-component.component.css']
})
export class MyComponentComponent implements OnInit {
  findWords: string;
  constructor(private service:MyServService) { }

  ngOnInit() {
    this.findWords = "";
  }

  onEnter(val){
    this.service.findWord(val).subscribe(x=>console.log(x));

    //this.findWords += val;
  }

}
