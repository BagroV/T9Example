import { ArrayType } from "@angular/compiler/src/output/output_ast";

export const Words = [
    {
        key: "A",
        values:["Animal","Apple"]
    },
    {
        key: "B",
        values:["Ball","Bottom"]
    },
    {
        key: "C",
        values:["Copper","cucumber"]
    },
];

export class Dict {
    key:number;
    values:Array<string>;
}

export const Dictionary:Dict[] = [
    {
        key: 2,
        values:["A","B","C"]
    },
    {
        key: 3,
        values:["D","E","F"]
    },
    {
        key: 4,
        values:["G","H","I"]
    },
    {
        key: 5,
        values:["J","K","L"]
    },
    {
        key: 6,
        values:["M","N","O"]
    },
    {
        key: 7,
        values:["P","Q","R","S"]
    },
    {
        key: 8,
        values:["T","U","V"]
    },
    {
        key: 9,
        values:["W","X","Y","Z"]
    },
];

